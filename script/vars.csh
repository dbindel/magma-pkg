# -- Load modules

module load magma-base
module load openmpi-1.8.6
module load gcc-4.9.3
module load openblas
module load lapack
#module load sprng
#module load gperftools
#module load tau
#module load ipm

#module load llvm
module load anaconda
#module load upc
#module load julia
