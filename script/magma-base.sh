MAGMA_BASE=$HOME/local
ANACONDA=$MAGMA_BASE/anaconda
LMOD=$MAGMA_BASE/lmod/lmod

# Clear system module setup

module purge
unset module

# Add anaconda base dirs

export PATH=$ANACONDA/bin${PATH:+":$PATH"}
export LIBRARY_PATH=$ANACONDA/lib${LIBRARY_PATH:+":$LIBRARY_PATH"}
export LD_RUN_PATH=$ANACONDA/lib${LD_RUN_PATH:+":$LD_RUN_PATH"}

# Add base directories

export PATH=$MAGMA_BASE/bin:$PATH
export LIBRARY_PATH=$MAGMA_BASE/lib:$MAGMA_BASE/lib64:$LIBRARY_PATH
export LD_RUN_PATH=$MAGMA_BASE/lib:$MAGMA_BASE/lib64:$LD_RUN_PATH

export CPATH=$MAGMA_BASE/include${CPATH:+":$CPATH"}
export MANPATH=$MAGMA_BASE/man${MANPATH:+":$MANPATH"}

export LUA_PATH="$MAGMA_BASE/share/lua/5.3/?.lua;$MAGMA_BASE/share/lua/5.3/?/init.lua"
export LUA_CPATH="$MAGMA_BASE/lib/lua/5.3/?.so"

# Add Lua module system

export MODULEPATH=$MAGMA_BASE/modules
source $LMOD/init/bash

# Add EasyBuild

#EBPATH=$HOME/.local/easybuild
#export MODULEPATH=$EBPATH/modules/all:$MODULEPATH
