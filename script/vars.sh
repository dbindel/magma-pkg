# -- Load default modules

module load gcc-4.9.3
module load openmpi-1.8.6
module load openblas
module load lapack
