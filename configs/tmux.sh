#!/bin/sh
#
# tmux -- a terminal multiplexer
# http://tmux.sourceforge/net/

source ./helper.sh
stage_dl_ac https://github.com/tmux/tmux/releases/download/2.0/tmux-2.0.tar.gz
