#/bin/sh
#
# livevent -- an event notification library
# http://libevent.org/

source ./helper.sh
stage_dl_ac https://sourceforge.net/projects/levent/files/libevent/libevent-2.0/libevent-2.0.22-stable.tar.gz
